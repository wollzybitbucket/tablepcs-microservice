from flask import Flask, request, jsonify


app = Flask(__name__)


def make_error(code, message):
    err = {
        'error': {
            'code': code,
            'message': message
        },
        'status': 'Error',
        'data': ''
    }
    return jsonify(err)


def get_serial_number_id_db(partner_id, client_id):
    return '13123123'


def get_client_id_db(partner_id, serial_number_id):
    return '1'


@app.route('/tablepcs/serialnumber', methods=['GET'])
def get_serial_number():
    if 'partner_id' in request.args and 'client_id' in request.args:
        serial_number_id = get_serial_number_id_db(request.args.get('partner_id'), request.args.get('client_id'))
        return jsonify({
            'status': 'Ok',
            'data': {
                'serial_number_id': serial_number_id
            }
        })
    else:
        return make_error(400, 'partner_id and client_id is required')


@app.route('/tablepcs/client', methods=['GET'])
def get_client():
    if 'partner_id' in request.args and 'serial_number_id' in request.args:
        client_id = get_client_id_db(request.args.get('partner_id'), request.args.get('serial_number_id'))
        return jsonify({
            'status': 'Ok',
            'data': {
                'client_id': client_id
            }
        })
    else:
        return make_error(400, 'partner_id and serial_number_id is required')



if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)