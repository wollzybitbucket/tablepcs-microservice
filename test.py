from main import app

import json
from unittest import TestCase, main, TextTestRunner


jwt_header = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjYXNoYm94X2lkIjoiNjc4NDY1MyIsInBhcnRuZXJfaWQiOiI3MzQ4NjczNDgiLCJ1c2VyX2lkIjoiMzI0MzI1In0.kNyJm8gXyNBPxUCYh_sA4THF9-ok9YV7kja1I0YGKs8'


class Tests(TestCase):
    
    def setUp(self):
        self.cli = app.test_client()

    def test_get_serial_number(self):
        response = self.cli.get('/tablepcs/serialnumber', query_string={'partner_id':'123','client_id':'312'})
        data = response.get_json()
        expected = {
            'status': 'Ok',
            'data': {
                'serial_number_id': '13123123'
            }
        }
        assert data == expected

        response = self.cli.get('/tablepcs/serialnumber')
        data = response.get_json()
        expected = {
            'error': {
                'code': 400,
                'message': 'partner_id and client_id is required'
            },
            'status': 'Error',
            'data': ''
        }
        assert data == expected

    def test_get_client(self):
        response = self.cli.get('/tablepcs/client', query_string={'partner_id':'123','serial_number_id':'312'})
        data = response.get_json()
        expected = {
            'status': 'Ok',
            'data': {
                'client_id': '1'
            }
        }
        assert data == expected

        response = self.cli.get('/tablepcs/client')
        data = response.get_json()
        expected = {
            'error': {
                'code': 400,
                'message': 'partner_id and serial_number_id is required'
            },
            'status': 'Error',
            'data': ''
        }
        assert data == expected

if __name__ == '__main__':
    log_file = 'test_log.txt'
    f = open(log_file, 'w')
    runner = TextTestRunner(f)
    main(testRunner=runner)
    f.close()

